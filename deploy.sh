#!/bin/bash

#To zip archive of root Nginx directory to home folder you have to use zip command.
#install zip with command $ sudo yum install zip
#The following command will zip files and directories:  $ zip -r <output_file> <folder_1> <folder_2> ... <folder_n>
#The < output_file> will be what you name the archive with .zip attatched to it.
#The default document Root location is  /usr/share/nginx/html/ on CentOS
#The config file is located at /etc/nginx/nginx.conf

zip -r nginx.conf_10_4_22.zip /etc/nginx/nginx.conf

#To retrieve updates to local git repository you have to make sure you are on the master branch.Run:  $git status, or $git log/origin
#Run the following: 

git pull

#don't forget to make script executable by running: 'chmod u+x deploy.sh'


